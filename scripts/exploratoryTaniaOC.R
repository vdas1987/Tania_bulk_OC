library(DESeq2)
library(pheatmap)
library(edgeR)
library(limma)
library(sva)
library(GTscripts)
#library(TCC)
library(gplots)


mono.OC.counts <- read.table("//Users/vdas/Documents/Test_lab_projects/Tania_Project_2017/data/salmon.genes.counts",header=T,row.names=1)
mono.OC.tpm <- read.table("/Users/vdas/Documents/Test_lab_projects/Tania_Project_2017/data/salmon.genes.tpm",header=T,row.names=1)
batch.mono.OC <- read.table("/Users/vdas/Documents/Test_lab_projects/Tania_Project_2017/data/batch.Tania.OC.txt",header=T,row.names=1)

keep <- rowSums(ovaca.counts)>0
ovaca.counts <- ovaca.counts[keep,]
ovaca.counts.round<-round(ovaca.counts)
dim(ovaca.counts.round)
#[1] 23294    35
## FI-FI-like removing the zero counts 
ovaca.counts.round.FI.FI_like<-ovaca.counts.round[,c("S_14_FI_001_S12915","S_15.FI.62_S14733","S_15_FI1_047_S12923","S_15_FI_001_S12924","S_16.FI2.09_S14727","S_16.FI2.10_S14726","S_16.FI2.19_S14728","S_16.FI2.20_S14729","S_15_O2_023_S12918","S_13.01.69_S16368","S_16.AS.O2_S14723","S_16AS20_S16367")]
keep <- rowSums(ovaca.counts.round.FI.FI_like)>0
ovaca.counts.round.FI.FI_like <- ovaca.counts.round.FI.FI_like[keep,]
batch.FI.FI_like<-batch.ovaca[colnames(ovaca.counts.round.FI.FI_like),] 
batch.FI.FI_like
dim(ovaca.counts.round.FI.FI_like)